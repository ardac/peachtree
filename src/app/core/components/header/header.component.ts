import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuVisible = false;

  @Output()
  readonly makeTransferButtonClicked: EventEmitter<unknown>;

  constructor() {
    this.makeTransferButtonClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onHamburgerButtonClick(): void {
    this.menuVisible = !this.menuVisible;
  }

  onMakeTransferButtonClick(): void {
    this.menuVisible = false;
    this.makeTransferButtonClicked.emit();
  }
}
