import { PtbComponentsModule } from 'ptb-components';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';



@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    PtbComponentsModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class CoreModule { }
