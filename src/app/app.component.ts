import { TransferInfo } from './../../projects/ptb-components/src/lib/components/transfer-form/types/transfer-info.interface';
import { TransferFormSubmittedEvent } from 'ptb-components';
import { Component, HostBinding, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'peachtree-bank-txs';

  // tslint:disable-next-line: variable-name
  private _shouldDisplayTransferFormOverlay = false;
  get shouldDisplayTransferFormOverlay(): boolean {
    return this._shouldDisplayTransferFormOverlay;
  }

  // tslint:disable-next-line: variable-name
  private _shouldDisplayConfirmationOverlay = false;
  get shouldDisplayConfirmationOverlay(): boolean {
    return this._shouldDisplayConfirmationOverlay;
  }

  onMakeTransferButtonClick(): void {
    this._shouldDisplayTransferFormOverlay = true;
  }

  onTransferFormSubmit(event: TransferFormSubmittedEvent): void {
    this._shouldDisplayTransferFormOverlay = false;
    this._shouldDisplayConfirmationOverlay = true;
  }

  onTransferFormCancel(): void {
    this._shouldDisplayTransferFormOverlay = false;
  }

  onTransferConfirmed(event: TransferInfo): void {
    this._shouldDisplayConfirmationOverlay = false;
  }

  onTransferCancelled(): void {
    this._shouldDisplayConfirmationOverlay = false;
  }
}
