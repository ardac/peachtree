import { Injectable } from '@angular/core';
import { CurrencySymbolResolver } from 'ptb-components';

@Injectable()
export class CurrenciesService implements CurrencySymbolResolver {

  constructor() { }

  getSymbolFor(p: { code: string }): string {
    const symbol = this.symbolsDictionary[p.code];
    return symbol || '';
  }

  // tslint:disable-next-line: member-ordering
  private readonly symbolsDictionary: { [key: string]: string } = {
    EUR: '€',
    USD: '$'
  };
}
