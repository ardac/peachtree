import { CurrenciesService } from './currencies.service';
import { Injectable } from '@angular/core';
import { SortOrder, Transaction, CurrencySymbolResolver } from 'ptb-components';
import { Observable, of } from 'rxjs';
import { TransactionsRequestParams } from '../types/transactions-request-params.interface';
import { TransactionResult } from '../types/transaction-result.interface';
import { TransactionRequest } from '../types/transaction-request.interface';

@Injectable()
export class BankingService {

  constructor(
    private readonly currency: CurrencySymbolResolver
  ) { }

  makeTransaction(p: TransactionRequest): Observable<TransactionResult> {
    const { beneficiary, amount, currencyCode } = p;
    if (this.mockBalance - amount < -500.) {
      return of({
        success: false,
        reason: 'Insufficient balance',
        balance: this.mockBalance,
        currencyCode: this.mockCurrencyCode
      });
    }

    this.mockBalance -= amount;
    const txnDate = Date.now();

    this.lastTxn = {
      categoryCode: '#1180aa',
      type: 'Transaction',
      date: new Date(txnDate),
      amount,
      currency: this.currency.getSymbolFor({ code: currencyCode }),
      beneficiary
    };

    this.mockTxns.push({
      categoryCode: '#1180aa',
      dates: {
        valueDate: txnDate
      },
      transaction: {
        amountCurrency: {
          amount,
          currencyCode
        },
        type: 'Transaction',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: beneficiary,
        accountNumber: ''
      }
    });

    return of({
      success: true,
      reason: '',
      balance: this.mockBalance,
      currencyCode: this.mockCurrencyCode
    });
  }

  getBalance(): Observable<number> {
    return of(this.mockBalance);
  }

  getTransactions(p: TransactionsRequestParams): Observable<Transaction[]> {
    const filtered = this.mockTxns
      .filter(mock => {
        if (!!p.filter) {
          const txnTypePass = (mock.transaction.type as string).toLocaleLowerCase().includes(p.filter.toLocaleLowerCase());
          const merchantPass = (mock.merchant.name as string).toLocaleLowerCase().includes(p.filter.toLocaleLowerCase());
          return txnTypePass || merchantPass;
        } else {
          return true;
        }
      }).map(
        mock => this.mockTxnMapper(mock)
      );

    if (!!p.sortBy && !!p.sortOrder) {
      filtered.sort((a, b) => {
        switch (p.sortBy) {
          case 'date':
            return this.compareDate(a.date, b.date, p.sortOrder);
            break;
          case 'beneficiary':
            return this.compareString(a.beneficiary, b.beneficiary, p.sortOrder);
            break;
          case 'amount':
            return this.compareNumber(a.amount, b.amount, p.sortOrder);
            break;
          default:
            break;
        }
        return 0;
      });
    }


    if (!!this.lastTxn) {
      const index = filtered.findIndex(txn => {
        for (const key in txn) {
          if (txn.hasOwnProperty(key)) {
            const a = (txn as any)[key];
            const b = (this.lastTxn as any)[key];
            if (key === 'date') {
              if ((a as Date).valueOf() !== (b as Date).valueOf()) {
                return false;
              }
            } else if ((txn as any)[key] !== (this.lastTxn as any)[key]) {
              return false;
            }
          }
        }
        return true;
      });
      if (index !== -1) {
        filtered.splice(index, 1);
      }
      filtered.unshift({ ...this.lastTxn });
      this.lastTxn = null;
    }

    return of(filtered);
  }

  getAllTransactions(): Observable<Transaction[]> {
    return of(
      this.mockTxns.map(mock => {
        return this.mockTxnMapper(mock);
      })
    );
  }

  private compareDate(a: Date, b: Date, order: SortOrder): number {
    return this.compareNumber(a.valueOf(), b.valueOf(), order);
  }

  private compareString(a: string, b: string, order: SortOrder): number {
    if (!order) {
      return 0;
    }
    const strA = a.toLocaleLowerCase();
    const strB = b.toLocaleLowerCase();
    if (order === 'asc') {
      if (strA < strB) {
        return -1;
      }
      if (strA > strB) {
        return 1;
      }
    } else if (order === 'desc') {
      if (strA > strB) {
        return -1;
      }
      if (strA < strB) {
        return 1;
      }
    }
    return 0;
  }

  private compareNumber(a: number, b: number, order: SortOrder): number {
    if (!order) {
      return 0;
    }
    if (order === 'asc') {
      return a - b;
    } else if (order === 'desc') {
      return b - a;
    } else {
      return 0;
    }
  }

  private mockTxnMapper(mock: any): Transaction {
    return {
      categoryCode: mock.categoryCode as string,
      type: mock.transaction.type as string,
      date: this.sanitizeDate(mock.dates.valueDate),
      amount: this.sanitizeAmount(mock.transaction.amountCurrency.amount as string | number),
      currency: this.currency.getSymbolFor({ code: mock.transaction.amountCurrency.currencyCode }),
      beneficiary: mock.merchant.name
    };
  }

  private sanitizeDate(date: string | number): Date {
      return new Date(date);
  }

  private sanitizeAmount(amount: string | number): number {
    if (!amount) {
      return 0;
    }
    if (typeof amount === 'number') {
      return amount;
    }
    const val = Number.parseFloat(amount);
    if (isNaN(val)) {
      return 0;
    }
    return val;
  }


  // tslint:disable: member-ordering
  private mockBalance = 5824.76;
  private mockCurrencyCode = 'USD';
  private lastTxn: Transaction | null = null;

  private readonly mockTxns: Array<any> = [
    {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1600493600000
      },
      transaction: {
        amountCurrency: {
          amount: 5000,
          currencyCode: 'EUR'
        },
        type: 'Salaries',
        creditDebitIndicator: 'CRDT'
      },
      merchant: {
        name: 'Backbase',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1600387200000
      },
      transaction: {
        amountCurrency: {
          amount: '82.02',
          currencyCode: 'EUR'
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'The Tea Lounge',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#d51271',
      dates: {
        valueDate: '2020-09-19'
      },
      transaction: {
        amountCurrency: {
          amount: '84.64',
          currencyCode: 'EUR'
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Texaco',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1600300800000
      },
      transaction: {
        amountCurrency: {
          amount: '84.76',
          currencyCode: 'EUR'
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'The Tea Lounge',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#c12020',
      dates: {
        valueDate: 1600370800000
      },
      transaction: {
        amountCurrency: {
          amount: '22.10',
          currencyCode: 'EUR'
        },
        type: 'Online Transfer',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Amazon Online Store',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#c89616',
      dates: {
        valueDate: 1600214400000
      },
      transaction: {
        amountCurrency: {
          amount: '46.25',
          currencyCode: 'EUR'
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: '7-Eleven',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#e25a2c',
      dates: {
        valueDate: 1476721442000
      },
      transaction: {
        amountCurrency: {
          amount: '19.72',
          currencyCode: 'EUR'
        },
        type: 'Online Transfer'
      },
      merchant: {
        name: 'H&M Online Store',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#1180aa',
      dates: {
        valueDate: '2020-09-15'
      },
      transaction: {
        amountCurrency: {
          amount: '68.87',
          currencyCode: 'EUR'
        },
        type: 'Transaction',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Jerry Hildreth',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#1180aa',
      dates: {
        valueDate: 1600041600000
      },
      transaction: {
        amountCurrency: {
          amount: '52.36',
          currencyCode: 'EUR'
        },
        type: 'Transaction',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Lawrence Pearson',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1599955200000
      },
      transaction: {
        amountCurrency: {
          amount: '75.93',
          currencyCode: 'EUR'
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Whole Foods',
        accountNumber: 'SI64397745065188826'
      }
    }, {
      categoryCode: '#fbbb1b',
      dates: {
        valueDate: 1599868800000
      },
      transaction: {
        amountCurrency: {
          amount: '142.95',
          currencyCode: 'EUR'
        },
        type: 'Online Transfer',
        creditDebitIndicator: 'DBIT'
      },
      merchant: {
        name: 'Southern Electric Company',
        accountNumber: 'SI64397745065188826'
      }
    }
  ];
}
