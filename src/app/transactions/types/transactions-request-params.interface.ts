import { SortOrder } from 'ptb-components';

export interface TransactionsRequestParams {
  filter: string;
  sortBy: string | null;
  sortOrder: SortOrder;
}
