export interface TransactionResult {
  success: boolean;
  reason: string;
  balance: number;
  currencyCode: string;
}
