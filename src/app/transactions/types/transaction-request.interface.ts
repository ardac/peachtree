export interface TransactionRequest {
  beneficiary: string;
  amount: number;
  currencyCode: string;
}
