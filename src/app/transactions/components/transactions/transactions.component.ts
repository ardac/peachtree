import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BankingService } from '../../services/banking.service';
import { TransferFormSubmittedEvent, TransferInfo, TransactionsRequestedEvent, Transaction } from 'ptb-components';
import { Component, EventEmitter, HostListener, Input, OnInit, Output, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { TransactionsRequestParams } from '../../types/transactions-request-params.interface';
import { map, shareReplay, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { TransferFormComponent } from 'projects/ptb-components/src/public-api';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy {

  @Input()
  shouldDisplayTransferFormOverlay = false;

  @Input()
  shouldDisplayConfirmationOverlay = false;

  @Input()
  transferFormVisibilityThreshold = 0;

  // tslint:disable-next-line: variable-name
  private _shouldDisplayTransferForm = true;
  get shouldDisplayTransferForm(): boolean {
    return this._shouldDisplayTransferForm;
  }

  @Output()
  readonly transferFormSubmitted: EventEmitter<TransferFormSubmittedEvent>;

  @Output()
  readonly transferFormCancelled: EventEmitter<unknown>;

  @Output()
  readonly transferConfirmed: EventEmitter<TransferInfo>;

  @Output()
  readonly transferCancelled: EventEmitter<unknown>;

  @ViewChildren('transferForm')
  transferForms!: QueryList<TransferFormComponent>;

  private readonly $transactionsListRequests: BehaviorSubject<TransactionsRequestParams>;
  readonly transactions$: Observable<Transaction[]>;

  private readonly $originAccountBalance: BehaviorSubject<number>;
  readonly originAccountBalance$: Observable<number>;

  private readonly $destroyed: Subject<unknown>;

  constructor(
    readonly banking: BankingService
  ) {
    this.transferFormSubmitted = new EventEmitter<TransferFormSubmittedEvent>();
    this.transferFormCancelled = new EventEmitter();
    this.transferConfirmed = new EventEmitter<TransferInfo>();
    this.transferCancelled = new EventEmitter();

    this.$destroyed = new Subject();

    this.$transactionsListRequests = new BehaviorSubject<TransactionsRequestParams>({
      filter: '',
      sortBy: null,
      sortOrder: null
    });
    this.transactions$ = this.$transactionsListRequests.pipe(
      switchMap(request => this.banking.getTransactions(request)),
      shareReplay(1)
    );

    this.$originAccountBalance = new BehaviorSubject<number>(0);
    this.originAccountBalance$ = this.$originAccountBalance.pipe(
      shareReplay(1)
    );
  }

  @HostListener('window:resize')
  onWindowResize(): void {
    this.determineTransferFormVisibility();
  }

  ngOnInit(): void {
    this.determineTransferFormVisibility();

    this.updateBalance();
  }

  ngOnDestroy(): void {
    this.$destroyed.next();
    this.$destroyed.complete();
  }

  // tslint:disable-next-line: member-ordering
  private transferInfo: TransferInfo | null = null;
  get submittedTransferInfo(): TransferInfo | null {
    return this.transferInfo;
  }

  onTransferFormSubmit(event: TransferFormSubmittedEvent): void {
    this.transferInfo = {
      beneficiaryAccount: event.beneficiaryAccount,
      amount: event.amount,
      currency: event.currency
    };
    this.transferFormSubmitted.emit(event);
  }

  onTransferFormCancel(): void {
    this.transferFormCancelled.emit();
  }

  onConfirmationSubmit(event: TransferFormSubmittedEvent): void {
    this.transferForms.forEach(form => form.reset());
    this.transferConfirmed.emit(
      !!this.transferInfo ? { ...this.transferInfo } : undefined
    );

    if (!!this.transferInfo) {
      this.banking.makeTransaction({
        beneficiary: this.transferInfo.beneficiaryAccount,
        amount: this.transferInfo.amount,
        currencyCode: this.transferInfo.currency
      }).pipe(
        tap(result => this.$originAccountBalance.next(result.balance)),
        withLatestFrom(this.$transactionsListRequests),
        tap(
          ([_, latestRequest]) => this.$transactionsListRequests.next(latestRequest)
        ),
        takeUntil(this.$destroyed)
      ).subscribe();
    }
    this.transferInfo = null;
  }

  onConfirmationCancel(): void {
    this.transferInfo = null;
    this.transferCancelled.emit();
  }

  onTransactionRequested(event: TransactionsRequestedEvent): void {
    this.$transactionsListRequests.next({
      filter: event.filter,
      sortBy: event.sortBy,
      sortOrder: event.sortOrder
    });
  }

  private determineTransferFormVisibility(): void {
    this._shouldDisplayTransferForm = window.innerWidth >= this.transferFormVisibilityThreshold;
  }

  private updateBalance(): void {
    this.banking.getBalance().pipe(
      takeUntil(this.$destroyed)
    ).subscribe(
      b => this.$originAccountBalance.next(b)
    );
  }
}
