import { CurrenciesService } from './services/currencies.service';
import { BankingService } from './services/banking.service';
import { ImageResolver, PtbComponentsModule, CurrencySymbolResolver } from 'ptb-components';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { ImagesService } from './services/images.service';



@NgModule({
  declarations: [
    TransactionsComponent
  ],
  imports: [
    CommonModule,
    PtbComponentsModule
  ],
  exports: [
    TransactionsComponent
  ],
  providers: [
    { provide: CurrencySymbolResolver, useClass: CurrenciesService },
    BankingService,
    { provide: ImageResolver, useClass: ImagesService },
  ]
})
export class TransactionsModule { }
