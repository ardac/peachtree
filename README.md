# PeachtreeBankTxs


## Structure

The application consist of a component library project and an application project.
Application project lives in the `./src/app` folder.
Component library project is located in the `./projects/ptb-components/src` folder.
## How to run

You need node.js installed on your machine - v14 should be fine.
After cloning the repository, navigate to the project folder (should be 'peachtree-bank-txs') in a command shell and run `npm i` - this might take a while to complete.

Then run `ng build ptb-components`, this will build the component library.

You can also run `ng test ptb-components` to run unit tests for the component library, running `ng test ptb-components --code-coverage --no-watch` will also output a code coverage report in `coverage` folder at the root project folder.

Run `ng serve` to fire up a dev server. Navigate to `http://localhost:4200/`. You should be able to see and interact with the running application.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

