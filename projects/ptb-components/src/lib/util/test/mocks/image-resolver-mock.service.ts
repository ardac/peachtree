import { Observable, of } from 'rxjs';

import { ImageResolver } from '../../../common/types/image-resolver.abstract';

export class ImageResolverMock extends ImageResolver {
  getSourceFor(p: { beneficiary: string }): Observable<string> {
    return of('');
  }
}
