import { CurrencySymbolResolver } from '../../../common/types/currency-symbol-resolver.abstract';

export class CurrencySymbolResolverMock extends CurrencySymbolResolver{
  getSymbolFor(p: { code: string; }): string {
    return '-';
  }
}
