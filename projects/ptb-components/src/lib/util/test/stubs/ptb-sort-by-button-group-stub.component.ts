import { Component, EventEmitter, Output } from '@angular/core';

import { SortOrderChangedEvent } from '../../../components/sort-by-button-group/types/sort-order-changed-event.interface';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-sort-by-button-group',
  template: ``
})
export class SortByButtonGroupStubComponent {
  @Output()
  sortOrderChange = new EventEmitter<SortOrderChangedEvent>();
}
