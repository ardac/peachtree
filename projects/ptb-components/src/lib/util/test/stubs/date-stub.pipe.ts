import { Pipe, PipeTransform } from '@angular/core';

export function fmtDate(date: Date): string {
  return date.toUTCString();
}

export const dateFmtSpy = jasmine.createSpy('fmtDate', fmtDate).and.callThrough();

@Pipe({ name: 'date' })
export class DateStubPipe implements PipeTransform {
  transform(date: Date, ...args: unknown[]): string {
    return dateFmtSpy(date);
  }
}
