import { Pipe, PipeTransform } from '@angular/core';

import { Transaction } from '../../../components/transaction-display/types/transaction.interface';

export function fmtAmount(amount: number): string {
  return amount.toFixed(5);
}

export const amountFmtSpy = jasmine.createSpy('fmtAmount', fmtAmount).and.callThrough();

@Pipe({ name: 'amount' })
export class TransactionAmountFormatterStubPipe implements PipeTransform {
  transform(txn: Transaction, ...args: unknown[]): string {
    return amountFmtSpy(txn.amount);
  }
}
