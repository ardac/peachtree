import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-plain-button',
  template: ``
})
export class PlainButtonStubComponent {
  @Output()
  clickk = new EventEmitter();

  @Input()
  disabled!: boolean;
}
