import { Component, Input } from "@angular/core";

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-sort-by-button',
  template: ``
})
export class SortByButtonStubComponent {
  @Input()
  id!: string | null;
}
