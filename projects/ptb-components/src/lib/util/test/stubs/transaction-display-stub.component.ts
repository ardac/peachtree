import { Component, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-transaction-display',
  template: ``
})
export class TransactionDisplayStubComponent {

  @Input()
  tabColor!: string | null;

  @Input()
  date!: string | null;

  @Input()
  imageSource!: string | null;

  @Input()
  beneficiary!: string | null;

  @Input()
  transactionType!: string | null;

  @Input()
  amount!: string | null;
}
