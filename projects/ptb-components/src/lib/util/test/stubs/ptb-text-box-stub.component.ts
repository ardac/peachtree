import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-text-box',
  template: ``,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextBoxStubComponent),
      multi: true
    }
  ]
})
export class TextBoxStubComponent {
  @Input()
  placeholder!: string;

  @Input()
  useClearButton!: boolean;

  @Input()
  readonly!: boolean;

  @Input()
  value!: string;

  @Input()
  prefix!: string;

  @Output()
  textChange = new EventEmitter<string>();

  writeValue(value: string): void {}
  registerOnChange(fn: (_: any) => void): void {}
  registerOnTouched(fn: () => void): void {}
  setDisabledState(isDisabled: boolean): void {}
}
