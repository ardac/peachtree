import { TransactionDisplayComponent } from './components/transaction-display/transaction-display.component';
import { NgModule } from '@angular/core';
import { TransactionsListComponent } from './components/transactions-list/transactions-list.component';
import { SortByButtonComponent } from './components/sort-by-button/sort-by-button.component';
import { CommonModule } from '@angular/common';
import { TextBoxComponent } from './components/text-box/text-box.component';
import { SortByButtonGroupComponent } from './components/sort-by-button-group/sort-by-button-group.component';
import { TransferFormComponent } from './components/transfer-form/transfer-form.component';
import { PlainButtonComponent } from './components/plain-button/plain-button.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TransactionAmountFormatterPipe } from './pipes/transaction-amount-formatter.pipe';



@NgModule({
  declarations: [
    TransactionDisplayComponent,
    TransactionsListComponent,
    TransferFormComponent,
    SortByButtonComponent,
    TextBoxComponent,
    SortByButtonGroupComponent,
    PlainButtonComponent,
    TransactionAmountFormatterPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    TransactionsListComponent,
    TransferFormComponent,
    SortByButtonComponent,
    TextBoxComponent,
    SortByButtonGroupComponent,
    PlainButtonComponent,
    TransactionAmountFormatterPipe
  ]
})
export class PtbComponentsModule { }
