export abstract class CurrencySymbolResolver {
  abstract getSymbolFor(p: { code: string }): string;
}
