import { Observable } from 'rxjs';

export abstract class ImageResolver {
  abstract getSourceFor(p: { beneficiary: string }): Observable<string>;
}
