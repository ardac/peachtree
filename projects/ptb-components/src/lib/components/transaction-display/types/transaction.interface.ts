export interface Transaction {
  categoryCode: string;
  type: string;
  date: Date;
  amount: number;
  currency: string;
  beneficiary: string;
}
