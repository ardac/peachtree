import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-transaction-display',
  templateUrl: './transaction-display.component.html',
  styleUrls: ['./transaction-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionDisplayComponent implements OnInit {

  @Input()
  tabColor = 'red';

  @Input()
  date = '';

  @Input()
  imageSource = '';

  @Input()
  beneficiary = '';

  @Input()
  transactionType = '';

  @Input()
  amount = '';

  constructor(
    private readonly elementRef: ElementRef
  ) {

  }

  ngOnInit(): void {
    this.elementRef.nativeElement.style.setProperty('--tab-color', this.tabColor);
  }

}
