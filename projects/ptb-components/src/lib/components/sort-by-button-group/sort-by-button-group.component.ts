import { SortOrderChangedEvent } from './types/sort-order-changed-event.interface';
import { SortByButtonComponent } from './../sort-by-button/sort-by-button.component';
import { AfterContentInit, Component, ContentChildren, EventEmitter, OnDestroy, OnInit, Output, QueryList } from '@angular/core';
import { Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-sort-by-button-group',
  templateUrl: './sort-by-button-group.component.html',
  styleUrls: ['./sort-by-button-group.component.scss']
})
export class SortByButtonGroupComponent implements OnInit, AfterContentInit, OnDestroy {

  @ContentChildren(SortByButtonComponent)
  buttons!: QueryList<SortByButtonComponent>;

  @Output()
  readonly sortOrderChange: EventEmitter<SortOrderChangedEvent>;

  private readonly $destroyed: Subject<unknown>;

  constructor() {
    this.$destroyed = new Subject();
    this.sortOrderChange = new EventEmitter<SortOrderChangedEvent>();
  }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    this.buttons.forEach(buttonOuterPass => {
      buttonOuterPass.clickk.pipe(
        tap(event => {
          this.buttons.forEach(buttonInnerPass => {
            buttonInnerPass.ambientSortOrder = event.sortOrder;
            if (buttonInnerPass.id !== buttonOuterPass.id) {
              buttonInnerPass.sortOrder = null;
            }
          });
        }),
        map(event => {
          return { orderBy: event.buttonId, order: event.sortOrder };
        }),
        takeUntil(this.$destroyed)
      ).subscribe(
        event => this.sortOrderChange.emit(event)
      );
    });
  }

  ngOnDestroy(): void {
    this.$destroyed.next();
    this.$destroyed.complete();
  }

}
