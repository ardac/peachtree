import { SortOrder } from '../../../common/types/sort-order.type';

export interface SortOrderChangedEvent {
  orderBy: string;
  order: SortOrder;
}
