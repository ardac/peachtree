import { SortOrderChangedEvent } from './types/sort-order-changed-event.interface';
import { SortByButtonComponent } from './../sort-by-button/sort-by-button.component';
import { DebugElement, Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortByButtonGroupComponent } from './sort-by-button-group.component';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'test-fixture',
  template: `
    <ptb-sort-by-button-group #group>
        <ptb-sort-by-button #first [id]="'first'"></ptb-sort-by-button>
        <ptb-sort-by-button #second [id]="'second'"></ptb-sort-by-button>
    </ptb-sort-by-button-group>
  `
})
class TestFixtureComponent {
  @ViewChild('group', { read: SortByButtonGroupComponent })
  buttonGroup!: SortByButtonGroupComponent;

  @ViewChild('first', { read: SortByButtonComponent })
  firstButton!: SortByButtonComponent;
  @ViewChild('second', { read: SortByButtonComponent })
  secondButton!: SortByButtonComponent;
}

describe('SortByButtonGroupComponent', () => {
  let testFixtureComponent: TestFixtureComponent;
  let testFixture: ComponentFixture<TestFixtureComponent>;
  let testFixtureComponentDebugEl: DebugElement;

  let buttonGroup: SortByButtonGroupComponent;
  let firstButton: SortByButtonComponent;
  let secondButton: SortByButtonComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestFixtureComponent, SortByButtonComponent, SortByButtonGroupComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    testFixture = TestBed.createComponent(TestFixtureComponent);
    testFixtureComponent = testFixture.componentInstance;
    testFixtureComponentDebugEl = testFixture.debugElement;
    testFixture.detectChanges();

    buttonGroup = testFixtureComponent.buttonGroup;
    firstButton = testFixtureComponent.firstButton;
    secondButton = testFixtureComponent.secondButton;

  });

  describe('When an inner button is clicked', () => {
    it('should set ambient sort order', () => {
      expect(firstButton.ambientSortOrder).toBe(null);
      expect(secondButton.ambientSortOrder).toBe(null);

      const sortOrderFirst = 'asc';
      firstButton.clickk.emit({ buttonId: 'first', sortOrder: sortOrderFirst });
      expect(secondButton.ambientSortOrder).toBe(sortOrderFirst);

      const sortOrderSecond = 'desc';
      secondButton.clickk.emit({ buttonId: 'second', sortOrder: sortOrderSecond });
      expect(firstButton.ambientSortOrder).toBe(sortOrderSecond);
    });
    it('should emit sort order change event with the id of the clicked button', () => {
      let eventEmitted!: SortOrderChangedEvent;
      buttonGroup.sortOrderChange.subscribe(
        (e: SortOrderChangedEvent) => eventEmitted = e
      );

      const firstButtonId = 'first';
      const sortOrderFirst = 'asc';
      firstButton.clickk.emit({ buttonId: firstButtonId, sortOrder: sortOrderFirst });
      expect(eventEmitted.orderBy).toBe(firstButtonId);

      const secondButtonId = 'first';
      const sortOrderSecond = 'asc';
      firstButton.clickk.emit({ buttonId: secondButtonId, sortOrder: sortOrderSecond });
      expect(eventEmitted.orderBy).toBe(secondButtonId);
    });
    it('should emit sort order change event with the sortOrder of the clicked button', () => {
      let eventEmitted!: SortOrderChangedEvent;
      buttonGroup.sortOrderChange.subscribe(
        (e: SortOrderChangedEvent) => eventEmitted = e
      );

      const firstButtonId = 'first';
      const sortOrderFirst = 'asc';
      firstButton.clickk.emit({ buttonId: firstButtonId, sortOrder: sortOrderFirst });
      expect(eventEmitted.order).toBe(sortOrderFirst);

      const secondButtonId = 'first';
      const sortOrderSecond = 'asc';
      firstButton.clickk.emit({ buttonId: secondButtonId, sortOrder: sortOrderSecond });
      expect(eventEmitted.order).toBe(sortOrderSecond);
    });
  });
});
