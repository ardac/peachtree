import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextBoxComponent),
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextBoxComponent implements OnInit, ControlValueAccessor, AfterViewInit {
  @ViewChild('input', { static: false })
  textInput!: ElementRef;

  @Input()
  prefix = '';

  @Input()
  placeholder = '';

  @Input()
  useClearButton = false;

  @Input()
  readonly = false;

  get clearButtonVisible(): boolean {
    if (!this.useClearButton) {
      return false;
    }
    return this.textInput && (this.textInput.nativeElement as HTMLInputElement).value !== '';
  }

  // tslint:disable-next-line: variable-name
  private _value = '';
  get value(): string {
    return this._value;
  }

  @Input()
  set value(inputValue: string) {
    this.writeValue(inputValue);
  }

  @Output()
  textChange: EventEmitter<string>;

  constructor() {
    this.textChange = new EventEmitter<string>();
  }

  //#region ControlValueAccessor implementation
  private onChange: (_: any) => void = () => { };
  private onTouched: () => void = () => { };

  writeValue(value: string): void {
    this._value = value;

    if (!!this.textInput) {
      (this.textInput.nativeElement as HTMLInputElement).value = value;
    }
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.readonly = isDisabled;
  }

  //#endregion

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    (this.textInput.nativeElement as HTMLInputElement).value = this.value;
  }

  onClearButtonClick(): void {
    if (!!this.textInput) {
      const el = this.textInput.nativeElement as HTMLInputElement;
      el.value = '';
      this.value = '';
      this.onChange(el.value);
      this.textChange.emit(el.value);
      el.focus();
    }
  }

  onInput(event: Event): void {
    const el = this.textInput.nativeElement as HTMLInputElement;
    this.onChange(el.value);
    this._value = el.value;
    this.textChange.emit(el.value);
  }

  onBlur(): void {
    this.onTouched();
  }

  onKeydown(event: KeyboardEvent): void {
    if (event.key !== 'Tab' && this.readonly) {
      event.preventDefault();
    }
  }

}
