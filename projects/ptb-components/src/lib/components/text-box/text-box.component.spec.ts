import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { runOnPushChangeDetection } from '../../util/test/run-onpush-change-detection.function';

import { TextBoxComponent } from './text-box.component';

describe('TextBoxComponent', () => {
  let component: TextBoxComponent;
  let fixture: ComponentFixture<TextBoxComponent>;
  let debugEl: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextBoxComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextBoxComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  });

  describe('when text is entered/changed', () => {
    let emittedEvent: string;
    let input: HTMLInputElement;
    const text = 'sample';
    beforeEach(() => {
      component.textChange.subscribe(
        (e: string) => emittedEvent = e
      );
      input = debugEl.query(By.css('input')).nativeElement;
    });
    it('should emit event containing the text', () => {
      input.value = text;
      input.dispatchEvent(new Event('input'));
      runOnPushChangeDetection(fixture);
      expect(emittedEvent).toBe(text);
    });
  });

  describe('When a prefix is provided', () => {
    const providedPrefix = '$';
    beforeEach(() => {
      component.prefix = providedPrefix;
    });

    it('the prefix should be displayed', () => {
      runOnPushChangeDetection(fixture);
      const displayedPrefix = debugEl.query(By.css('.prefix'));
      expect(displayedPrefix).toBeTruthy();
    });
  });

  describe('When clear button is set to be disabled/not used', () => {
    beforeEach(() => {
      component.useClearButton = false;
    });
    describe('and there is text input', () => {
      beforeEach(() => {
        component.writeValue('some text input');
      });
      it('clear button should NOT be visible', () => {
        runOnPushChangeDetection(fixture);
        const clearButton = debugEl.query(By.css('.clear-button'));
        expect(clearButton).toBeNull();
      });
    });
  });

  describe('When clear button is set to be enabled/used', () => {
    beforeEach(() => {
      component.useClearButton = true;
    });
    describe('and there is NO text input', () => {
      beforeEach(() => {
        component.value = '';
      });
      it('clear button should NOT be visible', () => {
        runOnPushChangeDetection(fixture);
        const clearButton = debugEl.query(By.css('.clear-button'));
        expect(clearButton).toBeFalsy();
      });
    });
    describe('and there IS text input', () => {
      let clearButton: HTMLElement;
      beforeEach(() => {
        component.value = 'some text input';
        runOnPushChangeDetection(fixture);
        clearButton = debugEl.query(By.css('.clear-button')).nativeElement;
      });
      it('clear button should be visible', () => {
        expect(clearButton).toBeTruthy();
      });
      describe('and when the clear button is clicked', () => {
        let emittedEvent: string;
        beforeEach(() => {
          component.textChange.subscribe(
            (e: string) => emittedEvent = e
          );
          clearButton.click();
          runOnPushChangeDetection(fixture);
        });
        it('text should be cleared', () => {
          expect(component.value).toBeFalsy();
        });
        it('emit text change event with empty text', () => {
          expect(emittedEvent).toBe('');
        });
      });
    });
  });
});
