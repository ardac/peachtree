import { SortOrder } from '../../../common/types/sort-order.type';

export interface SortByButtonClickedEvent {
  readonly buttonId: string;
  readonly sortOrder: SortOrder;
}
