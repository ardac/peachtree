import { Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { SortByButtonClickedEvent } from './types/sort-by-button-clicked-event.interface';
import { SortOrder } from '../../common/types/sort-order.type';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-sort-by-button',
  templateUrl: './sort-by-button.component.html',
  styleUrls: ['./sort-by-button.component.scss']
})
export class SortByButtonComponent implements OnInit {

  ambientSortOrder: SortOrder = null;

  @Input()
  id = '';

  @Input()
  set sortOrder(value: SortOrder) {
    this.$sortOrder.next(value);
  }

  @Output()
  clickk: EventEmitter<SortByButtonClickedEvent>;

  private readonly $sortOrder: BehaviorSubject<SortOrder>;
  readonly sortOrder$: Observable<SortOrder>;

  constructor() {
    this.clickk = new EventEmitter<SortByButtonClickedEvent>();

    this.$sortOrder = new BehaviorSubject<SortOrder>(null);
    this.sortOrder$ = this.$sortOrder.pipe(shareReplay(1));
  }

  @HostBinding('class.active')
  get isActive(): boolean {
    return !!this.$sortOrder.value;
  }

  @HostBinding('attr.tabindex')
  tabIndex = 0;

  @HostListener('click')
  @HostListener('keyup.enter')
  @HostListener('keyup.space')
  onClick(): void {
    const currSortOrder = this.$sortOrder.value;
    switch (currSortOrder) {
      case null:
        if (!!this.ambientSortOrder) {
          this.$sortOrder.next(this.ambientSortOrder);
        } else {
          this.$sortOrder.next('asc');
        }
        break;
      case 'desc':
        this.$sortOrder.next('asc');
        break;
      case 'asc':
        this.$sortOrder.next('desc');
        break;
      default:
        break;
    }

    this.clickk.emit({
      buttonId: this.id,
      sortOrder: this.$sortOrder.value
    });
  }

  ngOnInit(): void {
  }

}
