import { SortByButtonClickedEvent } from './types/sort-by-button-clicked-event.interface';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortByButtonComponent } from './sort-by-button.component';
import { SortOrder } from '../../common/types/sort-order.type';
import { DebugElement } from '@angular/core';

describe('SortByButtonComponent', () => {
  let component: SortByButtonComponent;
  let debugEl: DebugElement;
  let fixture: ComponentFixture<SortByButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SortByButtonComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortByButtonComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('if there is no ambient sort order', () => {
    describe('and button has no sort order set', () => {
      describe('when clicked successively', () => {
        describe('emitted events', () => {
          it('should contain \'asc\', \'desc\', \'asc\'... in order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('asc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('desc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('asc');
          });
        });
      });
    });
    describe('and button has \'asc\' set as it\'s sort order', () => {
      describe('when clicked successively', () => {
        describe('emitted events', () => {
          it('should contain \'desc\', \'asc\', \'desc\'... in order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            component.sortOrder = 'asc';

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('desc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('asc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('desc');
          });
        });
      });
    });
    describe('and button has \'desc\' set as it\'s sort order', () => {
      describe('when clicked successively', () => {
        describe('emitted events', () => {
          it('should contain \'asc\', \'desc\', \'asc\'... in order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            component.sortOrder = 'desc';

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('asc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('desc');

            debugEl.triggerEventHandler('click', null);
            expect(emittedOrder).toEqual('asc');
          });
        });
      });
    });
  });

  describe('if ambient sort order is \'asc\'', () => {
    describe('when clicked successively', () => {
      describe('emitted events', () => {
        it('should contain \'asc\', \'desc\', \'asc\'... in order', () => {
          let emittedOrder!: SortOrder;
          component.clickk.subscribe(
            (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
          );

          component.ambientSortOrder = 'asc';

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('asc');

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('desc');

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('asc');
        });
      });
    });
  });

  describe('if ambient sort order is \'desc\'', () => {
    describe('when clicked successively', () => {
      describe('emitted events', () => {
        it('should contain \'desc\', \'asc\', \'desc\'... in order', () => {
          let emittedOrder!: SortOrder;
          component.clickk.subscribe(
            (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
          );

          component.ambientSortOrder = 'desc';

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('desc');

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('asc');

          debugEl.triggerEventHandler('click', null);
          expect(emittedOrder).toEqual('desc');
        });
      });
    });
  });

  describe('when clicked', () => {
    let eventEmitted: boolean;
    let assignedId: string;

    beforeEach(() => {
      eventEmitted = false;
      assignedId = (Math.random() * 100).toFixed(0);
    });

    it('should emit event', () => {

      component.clickk.subscribe(() => eventEmitted = true);

      debugEl.triggerEventHandler('click', null);

      expect(eventEmitted).toBeTruthy();
    });

    describe('emitted event', () => {
      it('should contain id assigned to the button', () => {
        let emittedId = '';
        component.id = assignedId;

        component.clickk.subscribe(
          (event: SortByButtonClickedEvent) => emittedId = event.buttonId
        );

        debugEl.triggerEventHandler('click', null);

        expect(emittedId).toEqual(assignedId);
      });
    });


    describe('if there is no ambient sort order', () => {
      describe('and button has no sort order set', () => {
        describe('emitted event', () => {
          it('should contain \'asc\' as sort order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            component.sortOrder = 'desc';
            debugEl.triggerEventHandler('click', null);

            expect(emittedOrder).toEqual('asc');
          });
        });
      });
      describe('and button has \'asc\' set as it\'s sort order', () => {
        describe('emitted event', () => {
          it('should contain \'desc\' as sort order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            component.sortOrder = 'asc';
            debugEl.triggerEventHandler('click', null);

            expect(emittedOrder).toEqual('desc');
          });
        });
      });
      describe('and button has \'desc\' set as it\'s sort order', () => {
        describe('emitted event', () => {
          it('should contain \'asc\' as sort order', () => {
            let emittedOrder!: SortOrder;
            component.clickk.subscribe(
              (event: SortByButtonClickedEvent) => emittedOrder = event.sortOrder
            );

            component.sortOrder = 'desc';
            debugEl.triggerEventHandler('click', null);

            expect(emittedOrder).toEqual('asc');
          });
        });
      });
    });
  });
});
