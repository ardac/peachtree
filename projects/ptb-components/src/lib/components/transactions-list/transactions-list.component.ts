import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';

import { Transaction } from '../transaction-display/types/transaction.interface';
import { TransactionsRequestedEvent } from './types/transactions-requested-event.interface';
import { SortOrderChangedEvent } from '../sort-by-button-group/types/sort-order-changed-event.interface';

import { ImageResolver } from '../../common/types/image-resolver.abstract';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionsListComponent implements OnInit {

  @Input()
  title = 'Transactions';

  @Input()
  searchBoxPlaceholderText = 'Search by typing...';

  @Input()
  sortButtonGroupTitle = 'Sort by';

  @Input()
  dateButtonText = 'Date';

  @Input()
  beneficiaryButtonText = 'Beneficiary';

  @Input()
  amountButtonText = 'Amount';

  private readonly $transactions: BehaviorSubject<ReadonlyArray<Transaction> | null>;
  readonly transactions$: Observable<ReadonlyArray<Transaction> | null>;
  @Input()
  set transactions(transactions: ReadonlyArray<Transaction> | null) {
    this.$transactions.next(transactions);
  }

  @Output()
  readonly requestTransaction: EventEmitter<TransactionsRequestedEvent>;

  constructor(
    private readonly images: ImageResolver
  ) {
    this.requestTransaction = new EventEmitter<TransactionsRequestedEvent>();

    this.$transactions = new BehaviorSubject<ReadonlyArray<Transaction> | null>(null);
    this.transactions$ = this.$transactions.pipe(
      tap(transactions => this.buildImageSource$s(transactions))
    );
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: member-ordering
  private request: TransactionsRequestedEvent = {
    filter: '',
    sortBy: null,
    sortOrder: null
  };

  onSortOrderChange(event: SortOrderChangedEvent): void {
    this.request.sortBy = event.orderBy;
    this.request.sortOrder = event.order;
    this.requestTransaction.emit({ ...this.request });
  }

  onFilterChange(filter: string): void {
    this.request.filter = filter;
    this.requestTransaction.emit({ ...this.request });
  }

  // tslint:disable-next-line: member-ordering
  readonly imageSource$s: { [key: string]: Observable<string> } = {};

  private buildImageSource$s(transactions: ReadonlyArray<Transaction> | null): void {
    for (const key in this.imageSource$s) {
      if (this.imageSource$s.hasOwnProperty(key)) {
        delete this.imageSource$s[key];
      }
    }
    if (!!transactions) {
      transactions.forEach(txn => {
        const beneficiary = txn.beneficiary;
        this.imageSource$s[beneficiary] = this.images.getSourceFor({ beneficiary }).pipe(
          shareReplay(1)
        );
      });
    }
  }
}
