import { DebugElement} from '@angular/core';
import { By } from '@angular/platform-browser';

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { runOnPushChangeDetection } from '../../util/test/run-onpush-change-detection.function';
import { TransactionDisplayStubComponent } from './../../util/test/stubs/transaction-display-stub.component';
import { TextBoxStubComponent } from '../../util/test/stubs/ptb-text-box-stub.component';
import { SortByButtonGroupStubComponent } from '../../util/test/stubs/ptb-sort-by-button-group-stub.component';
import { ImageResolverMock } from './../../util/test/mocks/image-resolver-mock.service';
import { TransactionAmountFormatterStubPipe, amountFmtSpy, fmtAmount } from '../../util/test/stubs/transaction-amount-formatter-stub.pipe';
import { DateStubPipe, dateFmtSpy, fmtDate } from './../../util/test/stubs/date-stub.pipe';

import { TransactionsListComponent } from './transactions-list.component';
import { ImageResolver } from '../../common/types/image-resolver.abstract';
import { SortOrder } from '../../common/types/sort-order.type';
import { TransactionsRequestedEvent } from './types/transactions-requested-event.interface';
import { SortByButtonStubComponent } from './../../util/test/stubs/ptb-sort-by-button-stub.component';
import { Transaction } from '../transaction-display/types/transaction.interface';

describe('TransactionsListComponent', () => {
  let component: TransactionsListComponent;
  let fixture: ComponentFixture<TransactionsListComponent>;
  let debugEl: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TextBoxStubComponent,
        TransactionsListComponent,
        SortByButtonGroupStubComponent,
        SortByButtonStubComponent,
        TransactionDisplayStubComponent,
        TransactionAmountFormatterStubPipe,
        DateStubPipe
      ],
      providers: [
        { provide: ImageResolver, useClass: ImageResolverMock }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsListComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when the filter text changes', () => {
    let filterInput: TextBoxStubComponent;
    beforeEach(() => {
      filterInput = debugEl.query(By.directive(TextBoxStubComponent)).componentInstance;
    });
    it('should emit a new event containing filter and sort options', fakeAsync(() => {
      const filterText = 'test sample';
      let emittedEvent!: TransactionsRequestedEvent;
      component.requestTransaction.subscribe(
        (e: TransactionsRequestedEvent) => emittedEvent = e
      );
      filterInput.textChange.emit(filterText);
      tick();

      expect(emittedEvent.filter).toBe(filterText);
    }));
  });

  describe('when the sort option changes', () => {
    let sortByButtonGroup: SortByButtonGroupStubComponent;
    beforeEach(() => {
      sortByButtonGroup = debugEl.query(By.directive(SortByButtonGroupStubComponent)).componentInstance;
    });
    it('should emit a new event containing filter and sort options', fakeAsync(() => {
      const sortOrder = { orderBy: 'crit', order: 'asc' as SortOrder };
      let emittedEvent!: TransactionsRequestedEvent;
      component.requestTransaction.subscribe(
        (e: TransactionsRequestedEvent) => emittedEvent = e
      );
      sortByButtonGroup.sortOrderChange.emit(sortOrder);
      tick();

      expect(emittedEvent.sortOrder).toBe(sortOrder.order);
      expect(emittedEvent.sortBy).toBe(sortOrder.orderBy);
    }));
  });

  describe('when given an array of transactions', () => {
    let mockTransactions: Transaction[];
    beforeEach(() => {
      mockTransactions = [
        {
          categoryCode: '0.CategoryCode',
          type: '0.type',
          date: new Date('2020-12-01'),
          amount: 100,
          currency: '0.currency',
          beneficiary: '0.beneficiary'
        }, {
          categoryCode: '1.CategoryCode',
          type: '1.type',
          date: new Date('2020-12-02'),
          amount: 200,
          currency: '1.currency',
          beneficiary: '1.beneficiary'
        }
      ];
      component.transactions = mockTransactions;
      runOnPushChangeDetection(fixture);
    });
    it('should display correct number of them', fakeAsync(() => {
      tick();
      const instances = debugEl.queryAll(By.directive(TransactionDisplayStubComponent));
      expect(instances.length).toBe(mockTransactions.length);
    }));
    it('should display them with correct values', fakeAsync(() => {
      tick();
      const instances = debugEl
        .queryAll(By.directive(TransactionDisplayStubComponent))
        .map(dEl => dEl.componentInstance);
      expect(
        checkIfMatch(mockTransactions[0], instances[0])
      ).toBeTrue();
      expect(
        checkIfMatch(mockTransactions[1], instances[1])
      ).toBeTrue();
      expect(amountFmtSpy).toHaveBeenCalled();
      expect(dateFmtSpy).toHaveBeenCalled();
    }));
  });
});

function checkIfMatch(
  txn: Transaction,
  displayInstance: TransactionDisplayStubComponent
): boolean {
  return (
    txn.categoryCode === displayInstance.tabColor &&
    txn.type === displayInstance.transactionType &&
    fmtDate(txn.date) === displayInstance.date &&
    fmtAmount(txn.amount) === displayInstance.amount &&
    txn.beneficiary === displayInstance.beneficiary
  );
}
