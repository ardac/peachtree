import { SortOrder } from '../../../common/types/sort-order.type';

export interface TransactionsRequestedEvent {
  filter: string;
  sortBy: string | null;
  sortOrder: SortOrder;
}
