import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { CurrencySymbolResolver } from '../../common/types/currency-symbol-resolver.abstract';

import { CurrencySymbolResolverMock } from '../../util/test/mocks/currency-symbol-resolver-mock.service';
import { runOnPushChangeDetection } from '../../util/test/run-onpush-change-detection.function';

import { PlainButtonStubComponent } from './../../util/test/stubs/plain-button-stub.component';
import { TextBoxStubComponent } from './../../util/test/stubs/ptb-text-box-stub.component';

import { TransferFormComponent } from './transfer-form.component';
import { TransferFormSubmittedEvent } from './types/transfer-form-submitted-event.interface';
import { TransferInfo } from './types/transfer-info.interface';

describe('TransferFormComponent', () => {
  let component: TransferFormComponent;
  let fixture: ComponentFixture<TransferFormComponent>;
  let debugEl: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        TransferFormComponent,
        TextBoxStubComponent,
        PlainButtonStubComponent
      ],
      providers: [
        FormBuilder,
        { provide: CurrencySymbolResolver, useClass: CurrencySymbolResolverMock }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('cancel button', () => {
    describe('when configured as visible', () => {
      let cancelButton: DebugElement;
      beforeEach(() => {
        component.cancelButtonVisible = true;
        runOnPushChangeDetection(fixture);
        cancelButton = debugEl.query(By.css('ptb-plain-button.cancel'));
      });
      it('should be visible', () => {
        expect(cancelButton).toBeTruthy();
      });
      describe('and when clicked', () => {
        let cancelEmitted = false;
        beforeEach(() => {
          component.cancelled.subscribe(
            () => cancelEmitted = true
          );
          (cancelButton.componentInstance as PlainButtonStubComponent)
            .clickk.emit();
          runOnPushChangeDetection(fixture);
        });
        it('should emit cancelled event', () => {
          expect(cancelEmitted).toBeTrue();
        });
      });
    });
    describe('when configured as not visible', () => {
      let cancelButton: DebugElement;
      beforeEach(() => {
        component.cancelButtonVisible = false;
        runOnPushChangeDetection(fixture);
        cancelButton = debugEl.query(By.css('ptb-plain-button.cancel'));
      });
      it('should not be visible', () => {
        expect(cancelButton).toBeFalsy();
      });
    });
  });

  describe('when given origin account name, balance and currency code', () => {
    const testName = 'sample name';
    const testBalance = 1000;
    const testCurrencCode = 'USD';
    const mockSymbol = '+%&';
    let symbolSpy: jasmine.Spy;
    let boxes: TextBoxStubComponent[] = [];
    beforeEach(() => {
      const symbolResolver = debugEl.injector.get(CurrencySymbolResolver);
      symbolSpy = spyOn(symbolResolver, 'getSymbolFor').and.returnValue(mockSymbol);

      component.originAccountName = testName;
      component.originAccountBalance = testBalance;
      component.originAccountCurrencyCode = testCurrencCode;
      runOnPushChangeDetection(fixture);

      boxes = debugEl
        .queryAll(By.directive(TextBoxStubComponent))
        .map(el => el.componentInstance);
    });
    afterEach(() => {
      symbolSpy.calls.reset();
    })
    it('should display the info', () => {
      const expectedOrirignAccInfo = testName + ' - ' + mockSymbol + testBalance.toFixed(2);
      expect(component.originAccountInfo).toBe(expectedOrirignAccInfo);
      expect(boxes[0].value).toBe(expectedOrirignAccInfo);
      expect(symbolSpy).toHaveBeenCalledWith({ code: testCurrencCode });
      expect(component.originAccountCurrencySymbol).toBe(mockSymbol);
    });
  });

  describe('when given transfer information', () => {
    const xfrInfo: TransferInfo = {
      currency: 'EUR',
      beneficiaryAccount: 'test bene',
      amount: 123456
    };
    let boxes: TextBoxStubComponent[] = [];
    let boxOneSpy: jasmine.Spy;
    let boxTwoSpy: jasmine.Spy;
    beforeEach(() => {
      boxes = debugEl
        .queryAll(By.directive(TextBoxStubComponent))
        .map(el => el.componentInstance);

      boxOneSpy = spyOn(boxes[1], 'writeValue');
      boxTwoSpy = spyOn(boxes[2], 'writeValue');

      component.transferInfo = xfrInfo;
      runOnPushChangeDetection(fixture);
    });
    afterEach(() => {
      boxOneSpy.calls.reset();
      boxTwoSpy.calls.reset();
    });
    it('should display the information', () => {
      expect(boxOneSpy).toHaveBeenCalledWith(xfrInfo.beneficiaryAccount);
      expect(boxTwoSpy).toHaveBeenCalledWith(xfrInfo.amount);
    });
  });

  describe('submit button', () => {
    let formValidSpy: jasmine.Spy;
    beforeEach(() => {
      formValidSpy = spyOnProperty(component.form, 'valid');
    });
    it('should be disabled when the form is invalid', () => {
      formValidSpy.and.returnValue(false);
      runOnPushChangeDetection(fixture);
      const submitButton = debugEl.query(
        By.css('ptb-plain-button.submit')
      ).componentInstance as PlainButtonStubComponent;
      expect(submitButton.disabled).toBeTrue();
    });
    it('should be enabled when the form is valid', () => {
      formValidSpy.and.returnValue(true);
      runOnPushChangeDetection(fixture);
      const submitButton = debugEl.query(
        By.css('ptb-plain-button.submit')
      ).componentInstance as PlainButtonStubComponent;
      expect(submitButton.disabled).toBeFalse();
    });

    describe('when clicked', () => {
      let submitButton: PlainButtonStubComponent;
      let submitEvent: TransferFormSubmittedEvent;
      const currency = 'EUR';
      const beneficiaryAccount = 'test bene';
      const amount = 123456;
      beforeEach(() => {
        submitButton = debugEl.query(
          By.css('ptb-plain-button.submit')
        ).componentInstance as PlainButtonStubComponent;

        component.submitted.subscribe(
          (e: TransferFormSubmittedEvent) => submitEvent = e
        );
      });
      it('should emit event containing transfer information', () => {
        component.form.patchValue({
          beneficiaryAccount, amount
        });
        component.originAccountCurrencyCode = currency;
        submitButton.clickk.emit();
        runOnPushChangeDetection(fixture);
        expect(submitEvent).toBeTruthy();
        expect(submitEvent.beneficiaryAccount).toBe(beneficiaryAccount);
        expect(submitEvent.currency).toBe(currency);
        expect(submitEvent.amount).toBe(amount);
      });
    });
  });
});
