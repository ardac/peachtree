import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TransferFormSubmittedEvent } from './types/transfer-form-submitted-event.interface';
import { TransferInfo } from './types/transfer-info.interface';

import { CurrencySymbolResolver } from '../../common/types/currency-symbol-resolver.abstract';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransferFormComponent implements OnInit {

  @Input()
  title = 'title_placeholder';

  @Input()
  originAccountLabel = 'origin_account_label_placeholder';

  @Input()
  originAccountName = 'origin_account_name_placeholder';

  @Input()
  originAccountBalance = 0;

  // tslint:disable-next-line: variable-name
  private _originAccountCurrencySymbol = '';
  @Input()
  get originAccountCurrencySymbol(): string {
    return this._originAccountCurrencySymbol;
  }

  // tslint:disable-next-line: variable-name
  private _originAccountCurrencyCode = '';
  @Input()
  set originAccountCurrencyCode(code: string) {
    this._originAccountCurrencyCode = code;
    this._originAccountCurrencySymbol = this.currencies.getSymbolFor({ code });
  }

  @Input()
  beneficiaryLabel = 'beneficiary_label_placeholder';

  @Input()
  amountLabel = 'amount_label_placeholder';

  @Input()
  submitButtonText = 'Submit';

  @Input()
  cancelButtonText = 'Cancel';

  @Input()
  cancelButtonVisible = false;

  @Input()
  set transferInfo(info: TransferInfo | null) {
    if (!!info) {
      this.originAccountCurrencyCode = info.currency;
      this.form.patchValue({
        beneficiaryAccount: info.beneficiaryAccount,
        amount: info.amount
      });
    } else {
      this.originAccountCurrencyCode = '';
      this.form.patchValue({
        beneficiaryAccount: '',
        amount: 0
      });
    }
  }

  @Input()
  readonly = false;

  get originAccountInfo(): string {
    return this.originAccountName + ' - ' + this.originAccountCurrencySymbol + this.originAccountBalance.toFixed(2);
  }

  get formSubmittable(): boolean {
    return this.form.valid;
  }

  @Output()
  submitted: EventEmitter<TransferFormSubmittedEvent>;

  @Output()
  cancelled: EventEmitter<unknown>;

  readonly form: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly currencies: CurrencySymbolResolver
  ) {
    this.submitted = new EventEmitter<TransferFormSubmittedEvent>();
    this.cancelled = new EventEmitter();

    this.form = this.formBuilder.group({
      beneficiaryAccount: ['', Validators.required],
      amount: ['', [Validators.required, Validators.pattern(/^\d+(?:[\.,]?\d+)?$/), Validators.min(0), Validators.max(999999999999)]]
    });
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const formValue = this.form.value;
    this.submitted.emit(
      {
        beneficiaryAccount: formValue.beneficiaryAccount,
        amount: this.sanitizeAmount(formValue.amount),
        currency: this._originAccountCurrencyCode
      }
    );
  }

  onCancel(): void {
    this.cancelled.emit();
  }

  reset(): void {
    this.form.reset();
  }

  private sanitizeAmount(amount: string | number): number {
    if (typeof amount === 'number') {
      return amount;
    }
    const sanitized = amount.replace(',', '.');
    return Number.parseFloat(sanitized);
  }
}
