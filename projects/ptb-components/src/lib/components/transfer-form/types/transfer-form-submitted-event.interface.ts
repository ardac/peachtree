import { TransferInfo } from './transfer-info.interface';
// tslint:disable-next-line: no-empty-interface
export interface TransferFormSubmittedEvent extends TransferInfo {}
