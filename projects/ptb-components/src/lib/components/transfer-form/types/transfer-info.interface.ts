export interface TransferInfo {
  beneficiaryAccount: string;
  amount: number;
  currency: string;
}
