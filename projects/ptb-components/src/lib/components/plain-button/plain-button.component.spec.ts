import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { PlainButtonComponent } from './plain-button.component';

describe('PlainButtonComponent', () => {
  let component: PlainButtonComponent;
  let fixture: ComponentFixture<PlainButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlainButtonComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlainButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when enabled', () => {
    it('should emit event when clicked', () => {
      let eventEmitted = false;

      fixture.componentInstance.disabled = false;
      fixture.componentInstance.clickk.subscribe(() => eventEmitted = true);

      fixture.debugElement.triggerEventHandler('click', null);

      expect(eventEmitted).toBeTruthy();
    });
  });

  describe('when disabled', () => {
    it('should NOT emit event when clicked', () => {
      let eventEmitted = false;

      fixture.componentInstance.disabled = true;
      fixture.componentInstance.clickk.subscribe(() => eventEmitted = true);

      fixture.debugElement.triggerEventHandler('click', null);

      expect(eventEmitted).toBeFalsy();
    });
  });

  it('should emit even when clicked', () => {
    let eventEmitted = false;

    fixture.componentInstance.clickk.subscribe(() => eventEmitted = true);

    fixture.debugElement.triggerEventHandler('click', null);

    expect(eventEmitted).toBeTruthy();
  });
});
