import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ptb-plain-button',
  templateUrl: './plain-button.component.html',
  styleUrls: ['./plain-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlainButtonComponent implements OnInit {

  @Input()
  @HostBinding('class.disabled')
  disabled = false;

  @Output()
  clickk: EventEmitter<unknown>;

  constructor() {
    this.clickk = new EventEmitter();
  }

  @HostBinding('attr.tabindex')
  tabIndex = 0;

  @HostListener('click')
  @HostListener('keyup.enter')
  @HostListener('keyup.space')
  onClick(): void {
    if (!this.disabled) {
      this.clickk.emit();
    }
  }

  ngOnInit(): void {
  }

}
