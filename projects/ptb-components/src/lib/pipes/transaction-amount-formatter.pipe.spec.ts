import { TransactionAmountFormatterPipe } from './transaction-amount-formatter.pipe';

describe('TransactionAmountFormatterPipe', () => {


  it('transforms transaction amount and currency', () => {
    const pipe = new TransactionAmountFormatterPipe();

    const transaction = {
      categoryCode: '',
      type: '',
      date: new Date(),
      amount: 500.125,
      currency: '$',
      beneficiary: ''
    };
    expect(pipe.transform(transaction)).toBe('-$500.13');
    transaction.amount = 500.124;
    expect(pipe.transform(transaction)).toBe('-$500.12');
  });
});
