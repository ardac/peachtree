import { Pipe, PipeTransform } from '@angular/core';
import { Transaction } from '../components/transaction-display/types/transaction.interface';

@Pipe({
  name: 'amount'
})
export class TransactionAmountFormatterPipe implements PipeTransform {

  transform(txn: Transaction, ...args: unknown[]): string {
    if (!txn) {
      return '';
    }
    return '-' + txn.currency + txn.amount.toFixed(2);
  }

}
